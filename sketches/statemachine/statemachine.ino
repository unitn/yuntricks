#include <Bridge.h>
#include <Console.h>

/* Preprocessor constants */
#define DELAY 200
#define BRIDGE_BUFSIZE 16
#define LED 13

/* Type defs */
// function pointer
typedef bool (*state_ptr_t)( unsigned long );

// these will become indexes of state functions in an array of function pointers
typedef enum {
  S_IDLE = 0,
  S_COUNT,
  S_RESET,
  S_INFO,
  S_DEFAULT
} StateCode;

// Struct holding all the state variables (i.e. vars shared amongst states)
typedef struct {
  unsigned long count;
  state_ptr_t exec;
  StateCode prev_state;
  unsigned long delay_ms;
}  StateVars;

/* Global vars (leading 'g_' for clarity) */
// the state:
StateVars g_state;
// the list of state functions. Note thet the entry at index S_DEFAULT points to the s_idle function;
// this allows to fallback to a known and safe mode on unexpected state index:
state_ptr_t g_state_list[S_DEFAULT+1] = {s_idle, s_count, s_reset, s_info, s_idle};

/* Setup things */
void setup() {
  /* HW pins */
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  
  /* state vars */
  g_state.count    = 0;
  g_state.exec     = &s_idle;
  g_state.delay_ms = DELAY;

  /* Interfaces */
  Bridge.begin();
  Bridge.put("state", String(S_IDLE));
  Bridge.put("count", String(g_state.count));
  Bridge.put("delay", String(DELAY));

  Console.begin();
  //while(!Console);

  Serial.begin(115200);
  //while (!Serial);
  //Serial.println("Welcome on test sketch!");
}

/* Main loop: runs indefinitely */
void loop() {
  /* statics. They keep their previous value after each iteration */
  static char state_buf[BRIDGE_BUFSIZE];
  static char delay_buf[BRIDGE_BUFSIZE];
  
  /* locals. Re-initialized at the beginning of each iteration */
  StateCode req_state;
  g_state.prev_state = (StateCode)atoi(state_buf);
  static unsigned int v = 0;
  char ch;

  /* reads Console inputs. Console listens on TCP port 6571 */
  if (Console.available() > 0) {
    ch = Console.read();
    switch(ch) {
    case '/':
      Serial.print("delay value: ");
      Serial.println(g_state.delay_ms);
      break;
    case '?':
      Serial.print("last status: ");
      Serial.println(g_state.prev_state);
      break;
    case '0'...'9': // Accumulates values
      v = v * 10 + (ch - '0');
      break;
    case 'd':
      g_state.delay_ms = v;
      Bridge.put("delay", String(v));
      v = 0;
      break;
    case 's':
      v = constrain(v, S_IDLE, S_DEFAULT);
      if (v != g_state.prev_state) {
        Serial.print("Switch to state ");
        Serial.println(v);
        Bridge.put("state", String(v));
        g_state.exec = g_state_list[v];
      }
      v = 0;
      break;
    } 
  }
  else {
    /* Gets Bridge locations */
    Bridge.get("state", state_buf, BRIDGE_BUFSIZE);
    req_state = (StateCode)atoi(state_buf);
    if (req_state != g_state.prev_state) {
      // Ensure that req_state falls in the interval [S_IDLE, S_DEFAULT]:
      req_state = constrain(req_state, S_IDLE, S_DEFAULT);
      g_state.exec = g_state_list[req_state];
    }
    else {
      // NOOP
    }
  
    Bridge.get("delay", delay_buf, BRIDGE_BUFSIZE);
    g_state.delay_ms = (unsigned long)atoi(delay_buf);
  }
  
  /* Run the state function */
  (*g_state.exec)(g_state.delay_ms);
  
  /* End of Main Loop */
}


bool
s_idle(unsigned long d) {
  digitalWrite(LED, LOW);
  delay(d);
  return true;
}

bool
s_count(unsigned long d) {
  digitalWrite(LED, HIGH);
  g_state.count++;
  Bridge.put("count", String(g_state.count));
  Serial.println(g_state.count);
  delay(d);
  return true;
}

bool
s_reset(unsigned long d) {
  g_state.count = 0;
  Bridge.put("count", String(g_state.count));

  Bridge.put("state", String(g_state.prev_state));
  g_state.exec = g_state_list[g_state.prev_state];
  delay(d);
  return true;
}

bool
s_info(unsigned long d) {
  Serial.print("delay value: ");
  Serial.println(g_state.delay_ms);
  Serial.print("last status: ");
  Serial.println(g_state.prev_state);
  Serial.print("count: ");
  Serial.println(g_state.count);
  
  Bridge.put("state", String(g_state.prev_state));
  g_state.exec = g_state_list[g_state.prev_state];
  delay(d);
  return true;
}



